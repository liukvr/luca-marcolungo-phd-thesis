# H.pluvialis genome assembly



#### Illumina dataset

Filtered reads have been assembled using [SPAdes v3.11.1](http://cab.spbu.ru/software/spades/) and [SOAPdenovo v2.04](https://github.com/aquaskyline/SOAPdenovo2) using different kmer length range from 41 to 107.



```bash
#SPAdes v3.11.1
$ spades.py --dataset spades_config.yaml -t 25 -m 400 --only-assembler -k  95,99,103,107    --tmp-dir temp_dir  -o Hp_illumina

#SOAPdenovo v2.04
$ SOAPdenovo-127mer all -o K77 -p 20 -K 77 -F -V -R -s config_file_soapdenovo2.config
```



#### PacBio dataset

PacBio subreads were assembled using [Canu v1.5](https://github.com/marbl/canu), [Falcon v1.4.4](https://github.com/PacificBiosciences/pb-assembly), [Flye v2.5](https://github.com/fenderglass/Flye) and [Wtdbg2 v2.5](https://github.com/ruanjue/wtdbg2)

```bash
#Canu v1.5 
$ canu -p H.pluvialis -d H.pluvialis_PacBio genomeSize=300m -pacbio-raw subreads.fasta

#Falcon v1.4.4
$ fc_run file.cgf

#Flye v2.5
$ flye --pacbio-raw subreads.fasta -g 300m --out-dir output_dir -t threads_nbumber -i 1

#Wtdbg2 v2.5
$ wtdbg2 -i subreads.fasta -o HP.wtdbg2 -t 30 -L 5000
$ wtpoa-cns -t 22 -i HP.wtdbg2.ctg.lay -fo HP.wtdbg2.fasta
```



#### Oxford Nanopore

Oxford nanopore subreads were assembled using [Canu v1.5](https://github.com/marbl/canu), [Shasta v2.5](https://github.com/chanzuckerberg/shasta), [Flye v2.5](https://github.com/fenderglass/Flye) and [Wtdbg2 v2.5](https://github.com/ruanjue/wtdbg2)

```bash
#Canu v1.5 
$ canu -p H.pluvialis -d H.pluvialis_ONT genomeSize=300m -nanopore-raw nanopore_reads.fastq

#Flye v2.5
$ flye --nanopore-raw nanopore_reads.fasta -g 300m --out-dir  out_dir -t threads_number -i 1

#Shasta
$ shasta-Linux-0.1.0 --input nanopore_reads.fasta --Reads.minReadLength 0

#Wtdbg2 v2.5
$ wtdbg2 -i nanopore.fasta -o HP.wtdbg2 -t 30 -L 0
```



#### Bionano Optical maps

De novo genome map assembly was performed using Bionano Access v1.4.1 exploiting [RefAligner v9232](https://bionanogenomics.com/support/software-downloads/)

```bash
#BspQI genome map assembly

$ python pipelineCL.py -l output' -t RefAligner/1.0 -C \ clusterArgumentsBG_saphyr_phi.xml' \
-b Hp_Bionano_BssSI_RawMolecules.bnx \
-y  -d -U -i 5 -F 1 -W 0.4 \
-a Bionano_configuration_file_map_assembly.xml \
-r reference.map \
-R 0.5 -f 0.2 -J 48 -j 60 -jp 240 -T 240 -N 6


#BssSI genome map assembly

$ python pipelineCL.py -l output -t RefAligner/1.0 -C \ clusterArgumentsBG_saphyr_phi.xml \
-b Hp_Bionano_BspQI_RawMolecules.bnx  \
-y  -d -U -i 5 -F 1 -W 0.4 \
-a Bionano_configuration_file_map_assembly.xml \
-r reference.map \
-R 0.5 -f 0.2 -J 48 -j 60 -jp 240 -T 240 -N 6
```

#### Assembly polishing


##### Pacbio dataset
PacBio reads data have been aligned to the assembly using [pbmm2](https://github.com/PacificBiosciences/pbmm2) and assembly polished using [GenomicConsensus](https://github.com/PacificBiosciences/GenomicConsensus) using Quiver  algorithm. 
##### Oxford nanopore dataset 
ONT reads have been aligned to the assembly using [Minimap2](https://github.com/lh3/minimap2) and polishing performed using [racon](https://github.com/isovic/racon) plus [medaka](https://nanoporetech.github.io/medaka/index.html) 
##### Illumina reads
Short reads data have been aligned to the assembly using [bwa mem v0.7.17](https://github.com/lh3/bwa)and consensus sequence generated using [pilon](https://github.com/broadinstitute/pilon)

```bash
#Polishing using PacBio dataset
$ bax2bam *.bax.h5
$ pbmm2  align  Hp.pb.flye.fasta.mmi list_of_reads.fofn aligned.bam
$ quiver aligned.bam -r draft_genome.fasta -o quiver_polished.fasta -o

#Polishing using ONT dataset
$ minimap2 -a -L -t threads_number -x map-ont draft_genome.fasta ont_reads.fasta > alignment.sam 
$ racon -m 8 -x -6 -g -8 -w 500 -t 30 reads alignment.sam draft_genome.fasta 2>log1 > racon_polished.fasta
$ medaka_consensus -I ont_reads.fasta  -d draft-assembly.fasta -o output_dir -t threads_number

#Polishing using Illumina dataset
$ bwa mem -t threads_number reference.fasta read1.fastq.gz read2.fastq.gz | samtools sort --threads 5 -o aligned.bam
$ java -Xmx160G  -jar pilon.jar --genome draft_genome.fasta --frags alignment.bam --output pilon_polished --outdir Pilon_out --verbose --changes --vcf --vcfqe --tracks --fix all --threads threads_number




```

#### Identification of base-level errors
Short reads data have been aligned to the assembly using [bwa mem v0.7.17](https://github.com/lh3/bwa), PCR duplicates removed using [picard v2.9.2](https://broadinstitute.github.io/picard/) and variants identified using [gatk v4.0.2.1](https://gatk.broadinstitute.org/hc/en-us)  

```
$ bwa mem -t threads_number draf_assembly.fasta read1.fastq.gz read2.fastq.gz |  samtools sort --threads 5 -o aligned.bam;

$ java -jar picard.jar MarkDuplicates VALIDATION_STRINGENCY=SILENT MAX_RECORDS_IN_RAM=4000000 INPUT=file.bam OUTPUT=alignment.rg.bam METRICS_FILE=duplicates.txt REMOVE_DUPLICATES=true CREATE_INDEX=true

$ java -jar gatk.jar -T IndelRealigner -R draf_assembly.fasta  -I alignment.rg.bam -targetIntervals alignment.intervals -o alignment.realigned.bam

$ java -jar gatk4.0.2.1.jar -R reference.fasta  -T UnifiedGenotyper aligned.bam -o snps.raw.vcf -stand_call_conf 50.0 -dcov 200 -glm BOTH

```



#### Merging long read-based assemblies

PacBio and Oxford nanopore assemblies showing best contiguity metrics and BUSCO score were merged using [QuickMerge v0.3](https://github.com/mahulchak/quickmerge). Alignment were performed using [MUMmer v4](http://mummer.sourceforge.net/)



```bash
$ nucmer -l 100 -prefix out  self_assembly.fasta hybrid_assembly.fasta

$ delta-filter -r -q -l 10000 out.delta > out.rq.delta

$ Quickmerge -d  alignment.delta -q  ../pacbio.polished.fasta   -r ../ont.polished.fasta  -hco 5.0 -c  1.5 -l 0 -ml 5000 -p out
```



#### Assembly scaffolding and anchoring

Scaffolding have been performed using linked reads, Hi-C library and Optical mapping using [Scaff10X](https://github.com/wtsi-hpag/Scaff10X), [3D-DNA pipeline](https://github.com/theaidenlab/3d-dna) and [Bionano Solve](https://bionanogenomics.com/support/software-downloads/)  respectively. 

```
#Linked read data
$ scaff10x -nodes threads_number -longread 1 -gap 100 -matrix 2000 -reads 6  -link 4 -score 20 -edge 50000 -file 1 -plot output.png -block 50000 -data input.dat draft_assembly.fasta 10x_scaffolded.fasta

#Hi-C 
$ juicer.sh -g draft_genome  -s Sau3AI -p draft_genome.chrSize -D juicer_script_dir

$ run-asm-pipeline.sh --editor-coarse-resolution 50000 --editor-coarse-region 100000 --editor-saturation-centile 40 --editor-repeat-coverage 10 -q 20  -r 5 --editor-fine-resolution 1000  Hp.pbFlye.ontFlye.QM.fasta merged_nodups.txt

#Bionano optical map
$ Rscript runTGH.R -R RefAligner  -b1 bionano_BssSI_assembly.cmap -b2 bionano_BspQI_assembly.cmap -N  draft_assembly.fasta -e1 GCTCTTC -e2 CACGAG -t cur_results.tar.gz -s status.txt -f  Bionano_configuration_file_hybridScaffold_two_enzymes.xml -O output
```



#### Assessment of assembly quality and identification of misassemby

Structural correctness was evaluate exploiting linked reads and optical mapping data. [Scaff10X](https://github.com/wtsi-hpag/Scaff10X)

```
#Linked read data
$ break10x -nodes threads_number -gap 100 -reads 5 -score 20 -cover 50 -ratio 15 -data input.dat hic_assembly.fasta corrected.fasta corrected

#Bionano optical map
$ Rscript runTGH.R -R RefAligner  -b1 541_EXP_REFINEFINAL1.cmap -b2 543_EXP_REFINEFINAL1.cmap -N  assembly.fasta -e1 CACGAG -e2 GCTCTTC -t cur_results.tar.gz -s status.txt -f  Bionano_configuration_file_hybridScaffold_two_enzymes.xml - output
```

