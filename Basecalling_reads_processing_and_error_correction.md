## Basecalling, reads processing and error correction 



### Pacific Bioscience reads manipulation

Each SMRTCell raw sequencing file (bas.h5) was converte uding [pbh5tools](https://github.com/PacificBiosciences/pbh5tools) 

```bash
$ bash5tools.py  --outFilePrefix output_name --readType subreads --outType fasta
```



### Oxford Nanopore Technology basecalling and reads filtering

Nanopore fast5 files were convertet in fastq file using Guppy v3.1.5. Subsequentely, sequencing adapters were removed using [Porechop](https://github.com/rrwick/Porechop) and fastq converted into fasta file using [seqtk](https://github.com/lh3/seqtk)



```bash
$ guppy_basecaller --cpu_threads_per_caller threads_number -c guppy_config_file -i fast5_folder --hp_correct TRUE -s fastq --num_callers 1

$ porechop ont_reads.fastq -o ont-reads_trimmed.fastq --format fastq --verbosity 3 --threads threads_number

$ seqtk seq -A ont-reads_trimmed.fastq > ont-reads_trimmed.fasta
```



### Error correction of Illumina reads



Illumina reads were searched for sequencing errors and corrected using BayesHammer module of [SPAdes toolkit](https://github.com/ablab/spades)

```bash
$ spades.py -o output_folder --tmp-dir temp_folder --only-error-correction -t 16 -m 256 --pe1-1 illumina_R1.fastq.gz --pe1-2 illumina_R2.fastq.gz
```
