#!/bin/bash

#SBATCH --job-name=basecalling
#SBATCH --output=MinION_pipeline.log
#SBATCH -c 48
#SBATCH --mem-per-cpu=2000
#SBATCH -p high



if [ $# -eq 0 ]; then
	echo -e "\n\tSYNTAX: $0 <RUN_name> <FLOWCELL> \n";
	exit;
fi

if [ ! -d fast5 ]; then
	echo -e "\n\tfast5 directory does not exist\n"
	exit;
fi

if [ -f log  ]; then
        rm log;
fi


function log {

        if [ "$2" == "start"  ]; then
                Start=$(date);
                echo -e  "$1\n\tStart:$(date)\n\tNode:$(hostname)" >> log
        elif [ "$2" == "end" ]; then
                End=$(date)
                echo -e  "\tEnd:$(date)" >> log
                echo -e  "\tDuration:$(date -d @$(( $(date -d "$End" +%s) - $(date -d "$Start" +%s) )) -u +'%H:%M:%S')\n" >> log
        else
                echo "can't write log file, exit";
                exit
        fi
}

FLOWCELL="$2";
KIT="$3";
THREADS=48;

if [ "$FLOWCELL" = "FLO-MIN106" ]; then
        CONFIG="/storage-large/homedirs/luca.marcolungo/script/ont-guppy-cpu/data/dna_r9.4.1_450bps_hac.cfg";   # FLOMIN106
elif [ "$FLOWCELL" = "FLO-MIN107" ]; then
        CONFIG="/storage-large/homedirs/luca.marcolungo/script/ont-guppy-cpu/data/dna_r9.5_450bps.cfg";          # FLOMIN107
else
        echo "Flocell non riconosciuta, exit!!";
        exit;
fi

module load Python/3.6.5 
module load seqtk

PYTHON="/cm/shared/apps/Python/3.6.5/bin/python";
PORECHOP="/home/luca.marcolungo/.local/bin/porechop";
GUPPY="/home/luca.marcolungo/script/ont-guppy-cpu/bin/guppy_basecaller";
FASTQQUAL="/storage-large/homedirs/luca.marcolungo/script/fastq_mean_quality.py";
NANOPLOT="/home/luca.marcolungo/.local/bin/NanoPlot";

### BASECALLING ###
log Basecalling start
$GUPPY --cpu_threads_per_caller $THREADS -c $CONFIG -i ./fast5  --hp_correct TRUE -s fastq --num_callers 1 

echo "Concatenate fastq started at $(date)"

cat fastq/*.fastq > fastq/all.fastq;
rm fastq/fastq*
log Basecalling end

### PORECHOP and convert fastq to fasta ###
mkdir fasta
log Removing_adapter start
$PORECHOP -i fastq/all.fastq -o fastq/all.trimmed.fastq --format fastq --verbosity 3 --threads $THREADS
rm fastq/all.fastq;
seqtk seq -A fastq/all.trimmed.fastq > fasta/all.trimmed.fasta	
/usr/bin/python2.7 $FASTQQUAL  < fastq/all.trimmed.fastq > fastq/all.trimmed.fastq.qual
gzip fastq/all.trimmed.fastq;
log Removing_adapter end

### PLOT RUN results ###
log Plotting_results start
$NANOPLOT -t $THREADS --verbose -o plots -p $1 --N50 --title $1 --summary fastq/sequencing_summary.txt
log Plotting_results end
