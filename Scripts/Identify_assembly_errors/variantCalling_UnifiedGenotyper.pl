#!/usr/bin/perl

use strict;
use warnings;

#
# SCRIPT VARS
#
my $GATK = '/storage-large/shared-folders/gr_delledonne/GenomeAnalysisTK-3.8-1-0/GenomeAnalysisTK.jar';
my @JAVA     = ( '/cm/shared/apps/java/jdk1.8.0_144/bin/java', '-Duser.country=EN' ,'-Duser.language=us','-Djava.io.tmpdir=/home/luca.marcolungo/TMP', '-Xmx6G' );
my $REF = '';

#
# read arguments:
# - list of bam files
# - design bed file
# - hg## for genome version (hg19/hg38)
#

my $PLOIDY = 2;
my @input  = ();
foreach my $file (@ARGV)
{
    if ( -f $file )
    {
        if ( $file =~ m/bam$/i )
        {
            push @input, '-I';
            push @input, $file;
            print STDERR qq|Using BAM file "$file".\n|;
        }
        elsif ( $file =~ m/bed$/i )
        {
            push @input, '-L';
            push @input, $file;
            print STDERR qq|Using BED file "$file".\n|;
        }
    
    	elsif ( $file =~ m/fasta$/i )
    	{
            $REF = $file;
    	}
    }
    elsif ( $file =~ m/^\d+$/ )
    {
        $PLOIDY = $file;
        push @input, '--sample_ploidy';
        push @input, $PLOIDY;
        print STDERR qq|Using PLOIDY=$file.\n|;
    }
}

#my $REF   = qq|$bundles/$VER/genome.fasta|;
#my $DBSNP = qq|$bundles/$VER/dbsnp138.vcf|;
#my $REF   = qq|$bundles/ucsc.hg19_simple.fasta|;


if ( defined $input[1] && -f $input[1] && -f $REF )
{
    #print STDERR qq|Starting unifiedGenotyper using reference $VER!!!\n|;
    print STDERR qq|REFERENCE: $REF\n|;
    system(
	@JAVA, 		'-jar',
        $GATK,       '-R',               $REF,
        '-T',           'UnifiedGenotyper', @input,
        '-o',
        'snps.raw.vcf', '-stand_call_conf', '50.0',
        '-dcov',        200,                '-glm',
        'BOTH'
    );
}
else
{
    my $nn = $0;
    $nn =~ s/^.*\///;
    print STDERR qq|SYNTAX:

- $nn <file1.bam> [... fileN.bam] <reference> [target.bed] [ploidy]

NOTE:
  - reference is "hg38" (default) or "hg19"
  - ploidy is a number (default is 2)

|;
}
