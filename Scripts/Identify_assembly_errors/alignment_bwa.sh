#!/bin/bash
#SBATCH --job-name=bwa
#SBATCH --output=bwa.log
#SBATCH -c 28
#SBATCH --mem-per-cpu=2000



module load bwa
module load samtools

id="id"
name="id"
PROC=$((28 - 7 ));
SAMTOOLS='samtools';
BWA='bwa';


############################ HELP
function usage () {
cat <<-END

Usage

   $0 <reference .fasta> <Read1 .fastq.gz> <Read2 .fastq.gz> <Alignment ID> [Optional Arguments]

Optional Argument:
   -n | --name           name to add to Read group        [id]
   -i | --id             id  to add to Read group         [id]
   -t | --threads        max number of threads to use     [all available threads]
   -h | --help           Show this help and exit
END
}
if [ $# -eq 0 ];then usage; exit; fi

########################### Check Required parameters
if [ -f $1 ] && [ -f $2 ] && [ -f $3 ]; then
	REF=$(realpath $1);
	READ1=$(realpath $2);
	READ2=$(realpath $3);
	PREFIX=$4;
else
	echo "Error with required parameters, exit!"
	exit;
fi

######################### Select Optional parameters
while [ "$1" != "" ]; do
    case $1 in
        -n | --name )          shift
                               name=$1
                                ;;
        -i | --id )            shift
                               id=$1
                                ;;
        -t | --threads )         shift
                               PROC=$1
                                ;;
        -h | --help )          usage
                               exit;
                                ;;
    esac
    shift
done

########################## Print Selected Optional parameter
function Parameter () {
cat <<-END

Parameter selected:
     Reference:  $REF
         Read1:  $READ1
         Read2:  $READ2
 Output prefix:  $PREFIX
            id:  $id
          name:  $name
   BWA threads:  $PROC 	 
END
}
Parameter;

########################## Start alignment

echo -e "\nCommand Line:\t$BWA mem -R $(echo "@RG\tID:$id\tPU:lane\tLB:$name\tSM:$name\tCN:CGF-ddlab\tPL:ILLUMINA") -t $PROC $1 $2 $3| $SAMTOOLS sort --threads 5 -o $PREFIX.bam;";
echo -e "\nBWA alignment started at $(date) on $(hostname)"; 
$BWA mem -R $(echo "@RG\tID:$id\tPU:lane\tLB:$name\tSM:$name\tCN:CGF-ddlab\tPL:ILLUMINA") -t $PROC $REF $READ1 $READ2| $SAMTOOLS sort --threads 7 -o $PREFIX.bam;

echo -e "BAM indexing started at $(date) on $(hostname)";
$SAMTOOLS index $PREFIX.bam;

echo -e "BAM flagstat started at $(date) on $(hostname)";
$SAMTOOLS flagstat  $PREFIX.bam > $PREFIX.bam.flagstat;

echo -e "BWA alignment ended at $(date) on $(hostname)";
