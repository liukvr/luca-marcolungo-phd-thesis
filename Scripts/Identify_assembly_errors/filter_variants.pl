#!/usr/bin/perl

use strict;
use warnings;

#
# SCRIPT VARS
#

my $REF = '';

my $BGZIP = 'bgzip';
my $TABIX = 'tabix';

my $GATK     = '/storage-large/shared-folders/gr_delledonne/GenomeAnalysisTK-3.8-1-0/GenomeAnalysisTK.jar';
my @JAVA     = ( '/cm/shared/apps/java/jdk1.8.0_144/bin/java','-Djava.io.tmpdir=/storage-large/homedirs/luca.marcolungo/TMP' ,'-Duser.country=EN' ,'-Duser.language=us', '-Xmx10G' );
my $GATK4 = '/storage-large/shared-folders/gr_delledonne/gatk-4.1.4.0/gatk-package-4.1.4.0-local.jar'; 

my $VCF = '';
my $DP  = 5;
my $BED = '';

#
# PARSE PARAMETERS
#
foreach my $f (@ARGV)
{
    if ( $f =~ m/^\d+$/ ) { $DP = $f; }
    elsif ( -f $f )
    {
        if    ( $f =~ m/\.vcf/i )  { $VCF = $f; }
        elsif ( $f =~ m/\.bed$/i ) { $BED = $f; }
        elsif ( $f =~ m/\.fasta/i )   { $REF = $f; }
    }
}

print STDERR qq|SYNTAX:
> $0 <file.vcf> [minDP] [regions.bed] [ref.fa]

 INPUT VCF FILE: $VCF
      REFERENCE: $REF
CURRENTS min DP: $DP
         DESIGN: $BED

|;

if ( $VCF ne '' )
{
    my @temp = ();

    my $vcf = $VCF;

    my $pref = $VCF;
    $pref =~ s/.vcf$//;

    #
    # divide snv & indels
    #
    system( @JAVA, '-jar', $GATK4, 'SelectVariants', '--select-type-to-include', 'SNP',
        '--output', "$pref-snps.vcf", '-V', $vcf );
    system( @JAVA,'-jar', $GATK4, 'SelectVariants', '--select-type-to-exclude', 'SNP',
        '--output', "$pref-indels.vcf", '-V', $vcf );

    push @temp, "$pref-snps.vcf";
    push @temp, "$pref-snps.vcf.idx";
    push @temp, "$pref-indels.vcf";
    push @temp, "$pref-indels.vcf.idx";

    #
    # filter snv
    #
    my @do = (
        @JAVA,'-jar', $GATK,                      '-R',
        $REF,                                     '-T',
        'VariantFiltration',                      '--variant',
        qq|$pref-snps.vcf|,                       '-o',
        qq|$pref-snps.filtered.vcf|,              '--clusterWindowSize',
        10,                                       '--filterExpression',
        'MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)', '--filterName',
        "HARD_TO_VALIDATE",                       '--filterExpression',
        "DP < $DP",                               '--filterName',
        'LowCoverage',                            '--filterExpression',
        'QUAL < 30.0',                            '--filterName',
        'VeryLowQual',                            '--filterExpression',
        'QD < 5.0',                               '--filterName',
        'LowQD',                                  '--filterExpression',
        'FS > 60.0',                              '--filterName',
        'StrandBias'
    );
    if ( $BED ne '' )
    {
        push @do, '-L';
        push @do, $BED;
    }
    system @do;

    #
    # filter indels
    #
    @do = (
        @JAVA,'-jar', $GATK,                      '-R',
        $REF,                                     '-T',
        'VariantFiltration',                      '--variant',
        qq|$pref-indels.vcf|,                     '-o',
        qq|$pref-indels.filtered.vcf|,            '--clusterWindowSize',
        10,                                       '--filterExpression',
        'MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)', '--filterName',
        'HARD_TO_VALIDATE',                       '--filterExpression',
        "DP < $DP",                               '--filterName',
        'LowCoverage',                            '--filterExpression',
        'QUAL < 30.0',                            '--filterName',
        'VeryLowQual',                            '--filterExpression',
        'QD < 5.0',                               '--filterName',
        'LowQD',                                  '--filterExpression',
        'FS > 200.0',                             '--filterName',
        'StrandBias'
    );
    if ( $BED ne '' )
    {
        push @do, '-L';
        push @do, $BED;
    }
    system @do;

    #
    # merging filtered snv & indel
    #
    print qq|Merging the filtered Variants!!!\n|;
    system(
        @JAVA, '-jar', $GATK4,                'MergeVcfs', '-I',
        qq|$pref-snps.filtered.vcf|, '-I',        qq|$pref-indels.filtered.vcf|,
        '-O', 'variants.filtered.vcf');

    push @temp, "$pref-snps.filtered.vcf";
    push @temp, "$pref-snps.filtered.vcf.idx";
    push @temp, "$pref-indels.filtered.vcf";
    push @temp, "$pref-indels.filtered.vcf.idx";

    #
    # select PASS variants
    #
    print qq|Select PASS variants\n|;
    system( @JAVA, '-jar', $GATK4, 'SelectVariants', '-R', $REF, '--variant',
        'variants.filtered.vcf',
        '--exclude-filtered', '-O', 'variants.selected.vcf'  );

    #
    # bgzip all
    #

    #system( $BGZIP, $ARGV[0] );
    #system( $TABIX, $ARGV[0] . '.gz' );
    print qq|Zip filtered variants\n|;
    system( $BGZIP, 'variants.filtered.vcf' );
    system( $TABIX, 'variants.filtered.vcf.gz' );

    print qq|Zip selected variants\n|;
    system( $BGZIP, 'variants.selected.vcf' );
    system( $TABIX, 'variants.selected.vcf.gz' );

    unlink(@temp);	
}
