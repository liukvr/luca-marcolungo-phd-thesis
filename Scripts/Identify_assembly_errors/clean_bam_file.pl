#!/usr/bin/perl


use strict;
use warnings;

my $dir    = '';
my $map    = '';
my $REF = '';

foreach my $p (@ARGV)
{
    if ( -d $p )
    {
        $dir = $p;
        chdir($dir) || die "Directory not valid!\n";
    }
    elsif ( -f $p )
    {
        if ( $p =~ m/bam$/i )
        {
            $map = $p;
        }
        elsif ( $p =~ m/fasta$/i )
        {
            $REF = $p;
        }
    }
}

if ( $dir eq '' || $map eq '' )
{
    my $nn = $0;
    $nn =~ s/^.*\///;
    print STDERR qq|
SYNTAX:

 - $nn <dir> <sorted.bam> [design.bed]

|;
    exit;
}

my @JAVA     = ( '/cm/shared/apps/java/jdk1.8.0_144/bin/java', '-Duser.country=EN' ,'-Duser.language=us', '-Xmx6G' , '-Djava.io.tmpdir=/home/denise.lavezzari/temp'); 
my $SAMTOOLS = '/cm/shared/apps/samtools/1.9/bin/samtools';
my $PICARD   = '/storage-large/shared-folders/gr_delledonne/script/picard.jar';
my $GATK     = '/storage-large/shared-folders/gr_delledonne/GenomeAnalysisTK-3.8-1-0/GenomeAnalysisTK.jar';
#
# START BAM CLEANING
#

    if ( !-f 'alignment.realigned.bam' )
    {

        if ( !-f 'alignment.rg.bam' )
        {
system(
    @JAVA,                          '-jar',
    $PICARD,                        'MarkDuplicates',
    'VALIDATION_STRINGENCY=SILENT', 'MAX_RECORDS_IN_RAM=4000000',
    "INPUT=$map",       'OUTPUT=alignment.rg.bam',
    'METRICS_FILE=duplicates.txt',  'REMOVE_DUPLICATES=true',
    'CREATE_INDEX=true'
);
        }

        &IndelRealigner( $dir, 'alignment.rg.bam' );

    }

#
# IndelRealigner
#
sub IndelRealigner
{
    my $dir = shift;
    my $bam = shift;

    print STDERR qq|RealignerTargetCreator started on directory $dir!!!\n|;

    my @c = (
        @JAVA, "-jar", $GATK, '-T', 'RealignerTargetCreator', '-R',
        $REF, '-I', $bam, '-o', 'alignment.intervals',
    );

    system @c if ( !-f 'alignment.intervals' );

    print STDERR qq|IndelRealigner started on directory $dir!!!\n|;
    @c = (
        @JAVA, "-jar", $GATK,              '-T',
        'IndelRealigner',      '-R',
        $REF,                  '-I',
        $bam,                  '-targetIntervals',
        'alignment.intervals',
        '-o',
        'alignment.realigned.bam'
    );
    system @c;
}
