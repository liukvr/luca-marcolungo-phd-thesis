# PhD thesis scripts and commands
This repository is intended to give open access to all scripts, command line and configuration files used in the PhD thesis of Luca Marcolungo "Bioinformatics approaches for hybrid de novo genome assembly" at the Functional genomics Laboratory of the University of Verona under the supervion of Prof. Massimo Delledonne
